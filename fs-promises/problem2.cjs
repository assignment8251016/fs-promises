const fs = require("fs/promises")
function problem2() {
    const filenames = ["uppercase.txt", "lowercasesplit.txt", "sorted.txt"];
    return {
        readFile,
        toUpperCase,
        toLowerCaseSentence,
        sortSentence,
        deleteFiles,

    }
}


function readFile(file) {
    return fs.readFile(file, 'utf8');
}

function toUpperCase() {
    return readFile("./lipsum.txt").then(data => data.toUpperCase());
}

function toLowerCaseSentence(filename) {
    return readFile("./" + filename).then(
        data => {
            let sentence = ""
            for (let index = 0; index < data.length; index++) {
                if (data[index] !== '.') {
                    sentence += data[index]
                } else {
                    sentence += '.\n'
                    index += 1;
                }
            }
            return sentence.toLowerCase();
        }
    );
}

function sortSentence(filename) {
    return readFile("./" + filename).then(data => {
        return data.split("\n").reduce((textfile, data) => {
            const words = [];
            let word = "";
            for (let index = 0; index < data.length; index++) {
                if (data[index] !== ' ' && data[index] !== '\n') {
                    word += data[index];
                } else {
                    words.push(word);
                    word = ""
                }
            }
            textfile +=  words.sort((a, b) => a < b ? -1 : a > b ? 1 : 0).join(" ")
            return textfile+"\n";

        },"");
    }
    )
}

function deleteFiles() {
    readFile('./filenames.txt').then(
        data => data.split("\n").forEach(filename => {
            if (filename.length > 0) {
                fs.unlink(filename);
            }
        })
    );
}

module.exports = problem2;