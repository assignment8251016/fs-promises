const problem2 = require("../problem2.cjs")();
const fs = require("fs/promises");
filenames = ["uppercase.txt", "lowercasesplit.txt", "sorted.txt"];
    (async () => {
        await problem2.toUpperCase()
            .then(data => fs.writeFile(filenames[0], data, { flag: 'w' }, err => console.log(err)))
            .then(() => fs.writeFile("filenames.txt", filenames[0] + "\n", { flag: 'w+' }, err => console.log(err)))



        await problem2.toLowerCaseSentence(filenames[0])
            .then(data => fs.writeFile(filenames[1], data, { flag: 'w' }, err => console.log(err)))
            .then(() => fs.writeFile("filenames.txt", filenames[1] + "\n", { flag: 'a' }, err => console.log(err)))


        await problem2.sortSentence(filenames[1])
            .then(data => fs.writeFile(filenames[2], data, { flag: 'w' }, err => console.log(err)))
            .then(() => fs.writeFile("filenames.txt", filenames[2] + "\n", { flag: 'a' }, err => console.log(err)))


        problem2.deleteFiles()
    })();
